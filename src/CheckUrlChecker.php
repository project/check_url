<?php

namespace Drupal\check_url;

/**
 * Checks URL.
 */
class CheckUrlChecker {

  /**
   * Check the URLs.
   */
  public static function checkUrlChecked($select_links) {

    $checker = array();

    foreach ($select_links as $select_link) {
      $service_urls = \Drupal::service('check_url.manager')->checkUrl($select_link->link);
      $select_link->code = $service_urls;

      $checker[] = ['url' => $select_link->link, 'code' => $service_urls];

      \Drupal::database()->update('check_url')
        ->fields([
          'code' => $service_urls,
        ])
        ->condition('id', $select_link->id, '=')
        ->execute();

    }

  }

  /**
   * Check if it was successfully.
   */
  public static function checkUrlCheckerFinished($success, $results, $operations) {

    if ($success) {
      $message = t('Finished successfully');
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);

  }

}
