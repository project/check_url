<?php

namespace Drupal\check_url\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\check_url\CheckUrlEntityInterface;

/**
 * Defines the Check URL entity.
 *
 * @ConfigEntityType(
 *   id = "check_url",
 *   label = @Translation("Check URL"),
 *   handlers = {
 *     "list_builder" = "Drupal\check_url\Controller\CheckUrlListBuilder",
 *     "form" = {
 *       "add" = "Drupal\check_url\Form\CheckUrlForm",
 *       "edit" = "Drupal\check_url\Form\CheckUrlForm",
 *       "delete" = "Drupal\check_url\Form\CheckUrlDeleteForm",
 *       "scan" = "Drupal\check_url\Form\CheckUrlScanForm",
 *       "check" = "Drupal\check_url\Form\CheckUrlCheckForm",
 *     }
 *   },
 *   config_prefix = "check_url",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "url",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/check_url/{check_url}",
 *     "delete-form" = "/admin/config/system/check_url/{check_url}/delete",
 *   }
 * )
 */
class CheckUrlEntity extends ConfigEntityBase implements CheckUrlEntityInterface {

  /**
   * The CheckUrl ID.
   *
   * @var string
   */
  public $id;

  /**
   * The CheckUrl label.
   *
   * @var string
   */
  public $label;

  /**
   * The CheckUrl url.
   *
   * @var string
   */
  public $url;

}
