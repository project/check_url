<?php

namespace Drupal\check_url;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides methods for CheckUrl.
 */
class CheckUrl implements CheckUrlInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Invokes any cron handlers implementing hook_cron.
   */
  public function checkUrl($url) {

    if ($url == FALSE) {
      return FALSE;
    }

    $uri = \curl_init($url);
    curl_setopt($uri, CURLOPT_NOBODY, TRUE);
    curl_exec($uri);
    $statusCode = curl_getinfo($uri, CURLINFO_HTTP_CODE);
    curl_close($uri);

    return $statusCode;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinksFromUrl($url, $base_url) {

    if ($url == '') {
      return FALSE;
    }

    $context = stream_context_create(array(
      'http' => array('ignore_errors' => TRUE),
    ));
    $uriContent = file_get_contents($url, FALSE, $context);
    if ($uriContent == '') {
      return FALSE;
    }

    libxml_use_internal_errors(TRUE);
    $dom = new \DOMDocument();
    if ($dom->loadHTML($uriContent) == FALSE) {
      return FALSE;
    }
    libxml_use_internal_errors(FALSE);

    $path = new \DOMXPath($dom);
    $hrefs = $path->evaluate("/html/body//a");

    $links = [];
    for ($i = 0; $i < $hrefs->length; $i++) {
      $href = $hrefs->item($i);
      $uri = $href->getAttribute('href');
      $uri = filter_var($uri, FILTER_SANITIZE_URL);
      if (strpos($uri, '#') === FALSE) {
        if (!filter_var($uri, FILTER_VALIDATE_URL) === FALSE) {
          $links[] = [
            'base_url' => $url,
            'link' => $uri,
            'code' => NULL,
          ];
        }
        else {
          $links[] = [
            'base_url' => $url,
            'link' => $base_url . $uri,
            'code' => NULL,
          ];
        }
      }
    }
    return $links;
  }

}
