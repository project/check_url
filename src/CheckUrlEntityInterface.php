<?php

namespace Drupal\check_url;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides and interface for CheckUrl.
 */
interface CheckUrlEntityInterface extends ConfigEntityInterface {

}
