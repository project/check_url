<?php

namespace Drupal\check_url\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the CheckUrl add and edit forms.
 */
class CheckUrlForm extends EntityForm {

  /**
   * Constructs an CheckUrlForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];

    $check_url = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $check_url->label(),
      '#description' => $this->t("Label for the Example."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $check_url->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$check_url->isNew(),
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#field_prefix' => $base_url,
      '#title' => $this->t('Start URL'),
      '#default_value' => $check_url->url,
      '#description' => $this->t("Example: /node/1"),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
    $this->entity->url = $base_url . $this->entity->url;
    $check_url = $this->entity;

    $status = $check_url->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label Check Url.', [
        '%label' => $check_url->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Check Url was not saved.', [
        '%label' => $check_url->label(),
      ]));
    }
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('check_url')
      ->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
