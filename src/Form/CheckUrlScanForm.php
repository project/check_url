<?php

namespace Drupal\check_url\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Builds the form to Scan an cron.
 */
class CheckUrlScanForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want Scan all URLs?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.check_url.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Scan');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];

    $url = array();
    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $entity_ids = $query->execute();
    foreach ($entity_ids as $entity_id) {
      $url[] = $base_url . '/node/' . $entity_id;
    }

    $query = \Drupal::entityQuery('check_url');
    $entity_ids = $query->execute();

    foreach ($entity_ids as $entity_id) {
      $entity = \Drupal::entityTypeManager()->getStorage('check_url')->load($entity_id);
      $url[] = $entity->url;
    }

    $urls = array();
    foreach ($url as $uri) {
      $return = \Drupal::service('check_url.manager')->getLinksFromUrl($uri, $base_url);
      $urls = array_merge($urls, $return);
    }

    $temp = new DrupalDateTime();
    $time = $temp->format('Y-m-d H:i:s');

    \Drupal::database()->truncate('check_url')->execute();

    foreach ($urls as $insert) {
      \Drupal::database()->insert('check_url')
        ->fields([
          'baseurl' => $insert['base_url'],
          'link' => $insert['link'],
          'code' => '0',
          'time' => $time,
          'errorcount' => '0',
        ])
        ->execute();
    }

    if ($urls != NULL) {
      $this->messenger()->addMessage($this->t('URLs are scanned'));

    }
    else {
      $this->messenger()->addMessage($this->t('URLs are not scanned'));
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
