<?php

namespace Drupal\check_url\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to Scan an cron.
 */
class CheckUrlCheckForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want Check all URLs?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.check_url.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Check');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $query = \Drupal::database()->select('check_url', 'link');
    $query->fields('link', ['id', 'link', 'code', 'baseurl']);

    $select_links = $query->execute()->fetchAll();

    $batch = array(
      'title' => t('Checking URLs...'),
      'operations' => array(
        array(
          '\Drupal\check_url\CheckUrlChecker::checkUrlChecked',
          array(
            $select_links,
          ),
        ),
      ),
      'finished' => '\Drupal\check_url\CheckUrlChecker::checkUrlCheckerFinished',
    );
    batch_set($batch);

    $form_state->setRedirectUrl($this->getCancelUrl());

  }

}
