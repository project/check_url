<?php

namespace Drupal\check_url\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * @file
 * Contains \Drupal\check_url\Plugin\QueueWorker\ExampleQueueWorker.
 */

/**
 * Processes tasks for check_url module.
 *
 * @QueueWorker(
 *   id = "check_url_queue",
 *   title = @Translation("Check Url: Queue worker"),
 *   cron = {"time" = 90}
 * )
 */
class CheckUrlQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $query = \Drupal::database()->select('check_url', 'link');
    $query->fields('link', ['id', 'link', 'code', 'baseurl']);

    $select_links = $query->execute()->fetchAll();

    $checker = array();

    foreach ($select_links as $select_link) {
      $service_urls = \Drupal::service('check_url.manager')->checkUrl($select_link->link);
      $select_link->code = $service_urls;

      $checker[] = ['url' => $select_link->link, 'code' => $service_urls];

      \Drupal::database()->update('check_url')
        ->fields([
          'code' => $service_urls,
        ])
        ->condition('id', $select_link->id, '=')
        ->execute();

      echo $select_link;

    }

  }

}
