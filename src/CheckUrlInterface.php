<?php

namespace Drupal\check_url;

/**
 * Provides and interface for CheckUrl.
 */
interface CheckUrlInterface {

  /**
   * Checks the URLs.
   */
  public function checkUrl($url);

  /**
   * Scanns the URLs.
   */
  public function getLinksFromUrl($exturl, $bace_url);

}
