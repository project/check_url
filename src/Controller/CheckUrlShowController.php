<?php

namespace Drupal\check_url\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Shows all URLs with an error, witch have to be scanned.
 */
class CheckUrlShowController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $query = \Drupal::database()->select('check_url', 'link');
    $query->fields('link', ['link', 'code', 'baseurl']);
    $group = $query->orConditionGroup()
      ->condition('code', 403, '=')
      ->condition('code', 404, '=')
      ->condition('code', 500, '=');

    $results = $query->condition($group)->execute()->fetchAll();

    $output = array();
    foreach ($results as $result) {
      $output[] = [
        'Link' => $result->link,
        'Code' => $result->code,
        'Baseurl' => $result->baseurl,
      ];
    }

    $page['table'] = [
      '#type' => 'table',
      '#header' => ['URLs', 'Status Code', 'Base URLs'],
      '#rows' => $output,
      '#cache' => ['max-age' => 0],
      '#empty' => t('No data found'),
    ];

    return $page;

  }

}
