<?php
namespace Drupal\check_url\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

class TmpController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {


    $select_links = db_select('check_url', 'link')
      ->fields('link')
      ->condition(
      db_or()
        ->condition('code', 301)
        ->condition('code', 404)
        ->condition('code', 500)
      )
      ->execute()
      ->fetchAll();


    $rows = array();
    foreach ($select_links as $select_link) {
      $rows[] = array($select_link->link, $select_link->code, $select_link->baseurl);
    }

    return array(
      '#type' => 'table',
      '#header' => array('URLs', 'Status Code', 'Base URLs'),
      '#rows' => $rows,
      '#cache' => array('max-age' => 0),
    );







    $select_links = db_select('check_url', 'link')
      ->fields('link')
      ->execute()
      ->fetchAll();

    echo 'Check the following links';
    echo  '<table><tr><th style="text-align: left">URLs</th><th style="text-align: left">Status Code</th><th style="text-align: left">Base URLs</th></tr>';

    foreach ($select_links as $select_link) {

      if ( $select_link->code == 403 or $select_link->code == 404 or $select_link->code == 500 ) {
        echo  '<tr><td>' . $select_link->link . '</td><td>'. $select_link->code . '</td><td>'. $select_link->baseurl . '</td></tr>';
      }
    }


    exit();


    //    $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
//
//    $url = array();
//    $query = \Drupal::entityQuery('node');
//    $query->condition('status', 1);
//    $entity_ids = $query->execute();
//    foreach ($entity_ids as $entity_id) {
//      $url[] = $base_url . '/node/' .$entity_id;
//    }
//
//    $urls = array();
//    foreach ($url as $uri) {
//      $return = \Drupal::service('check_url.manager')->getLinksFromUrl($uri, $base_url);
//      $urls = array_merge($urls, $return);
//    }
//
//    $temp = new \Drupal\Core\Datetime\DrupalDateTime();
//    $time = $temp->format('Y-m-d H:i:s');
//
//    db_truncate('check_url')
//      ->execute();
//
//    foreach ($urls as $insert){
//      db_insert('check_url')
//        ->fields([
//          'baseurl' => $insert['base_url'],
//          'link' => $insert['link'],
//          'code' => '0',
//          'time' => $time,
//          'errorcount' => '0',
//        ])
//        ->execute();
//    }

  }
}
