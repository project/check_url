<?php

namespace Drupal\check_url\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Shows all URLs, witch have to be scanned.
 */
class CheckUrlShowAllController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $query = \Drupal::database()->select('check_url', 'link');
    $query->fields('link', ['link', 'code', 'baseurl']);

    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
    $results = $pager->execute()->fetchAll();

    $output = array();
    foreach ($results as $result) {
      $output[] = [
        'Link' => $result->link,
        'Code' => $result->code,
        'Baseurl' => $result->baseurl,
      ];
    }

    $page['table'] = [
      '#type' => 'table',
      '#header' => ['URLs', 'Status Code', 'Base URLs'],
      '#rows' => $output,
      '#cache' => ['max-age' => 0],
      '#empty' => t('No data found'),
    ];

    $page['pager'] = array(
      '#type' => 'pager',
    );

    return $page;

  }

}
